package main;

public class Библиотека_Math {

    public void task1(double a, float v) {
        a *= Math.PI / 180;
        float g = 9.81f;

        double t1 = (float) 2 * v * Math.sin(a);
        double t = t1 / g;

        float l = (float) (2 * Math.pow(v, 2) * Math.cos(a) * Math.sin(a) / g);

        float gravity = 2 * g;
        float sin1 = (float) Math.sin(a);
        float sin = (float) Math.pow(sin1, 2.0);
        float h1 = (float) (Math.pow(v, 2));
        float h2 = h1 / gravity;
        float h = h2 * sin;
        System.out.println("Дальность = " + l);
        System.out.println("Время полета = " + t);
        System.out.println("Высота полета = " + h);
    }

    public void task2(float v1, float v2, float S, int hour, int min, int sec) {
        int day = 0;
        float t = hour + (float) min / 60 + (float) sec / 3600;
        float s1 = v1 * t;
        float s2 = v2 * t;
        float S_all = s1 + s2 + S;


        if (sec >= 60) {
            min += sec / 60;
            sec = sec % 60;
        }
        if (min >= 60) {
            hour += min / 60;
            min = min % 60;
        }
        if (hour >= 24) {
            day += hour / 24;
            hour = hour % 24;
        }

        if (hour != 0) {
            if (day == 0 && min == 0 && sec == 0) {
                System.out.println("Дистанция между машинами через " + hour + " часа  = " + S_all);
            } else if (day != 0 && min == 0 && sec == 0) {
                System.out.println("Дистанция между машинами через " + day + " день " + hour + " часа  = " + S_all);
            }
            if (day == 0 && min != 0 && sec == 0) {
                System.out.println("Дистанция между машинами через " + hour + " часа " + min + " минут " + " = " + S_all);
            } else if (day != 0 && min != 0 && sec == 0) {
                System.out.println("Дистанция между машинами через " + day + " день " + hour + " часа " + min + " минут " + " = " + S_all);
            }
            if (day == 0 && min != 0 && sec != 0) {
                System.out.println("Дистанция между машинами через " + hour + " часа " + min + " минут " + sec + " секунд " + " = " + S_all);
            } else if (day != 0 && min != 0 && sec != 0) {
                System.out.println("Дистанция между машинами через " + day + " день " + hour + " часа " + min + " минут " + sec + " секунд " + " = " + S_all);
            }
            if (day == 0 && min == 0 && sec != 0) {
                System.out.println("Дистанция между машинами через " + hour + " часа " + sec + " секунд " + " = " + S_all);
            } else if (day != 0 && min == 0 && sec != 0) {
                System.out.println("Дистанция между машинами через " + day + " день " + hour + " часа " + sec + " секунд " + " = " + S_all);
            }
        }
        if (hour == 0) {
            if (day == 0 && min == 0 && sec == 0) {
                System.out.println("Дистанция между машинами через = " + S_all);
            } else if (day != 0 && min == 0 && sec == 0) {
                System.out.println("Дистанция между машинами через " + day + " день  = " + S_all);
            }
            if (day == 0 && min != 0 && sec == 0) {
                System.out.println("Дистанция между машинами через " + min + " минут " + " = " + S_all);
            } else if (day != 0 && min != 0 && sec == 0) {
                System.out.println("Дистанция между машинами через " + day + " день " + min + " минут " + " = " + S_all);
            }
            if (day == 0 && min != 0 && sec != 0) {
                System.out.println("Дистанция между машинами через " + min + " минут " + sec + " секунд " + " = " + S_all);
            } else if (day != 0 && min != 0 && sec != 0) {
                System.out.println("Дистанция между машинами через " + day + " день " + min + " минут " + sec + " секунд " + " = " + S_all);
            }
            if (day == 0 && min == 0 && sec != 0) {
                System.out.println("Дистанция между машинами через " + +sec + " секунд " + " = " + S_all);
            } else if (day != 0 && min == 0 && sec != 0) {
                System.out.println("Дистанция между машинами через " + day + " день " + sec + " секунд " + " = " + S_all);
            }
        }
    }

    public void task3(double x, double y) {
        boolean be = false;
        if(y>=0 && y<=-x && x>=-((y-3)/2)*x-1 || y<=0 && y<=-x && x>=-((y-3)/2)*x-1){
            be = true;
        }
        if(y>=0 && y<=x && x>=((y-3)/2)*x-1 || y<=0 && y<=x && x>=((y-3)/2)*x-1){
            be = true;
        }
        System.out.println(be);
    }
    public void task4(double x){
        double result=0;
        double ex1 = Math.exp(x+1)+2*Math.exp(x);
        double q = x;
        double ex2 = Math.exp(x+1);
        x *= Math.PI / 180;
        double sin = Math.sin(x);
        double cos = Math.cos(x);
        ex1= ex1 * cos;
        ex2 = q-ex2*sin;
        ex1 = Math.sqrt(ex1);
        ex1 = Math.log10(Math.sqrt(ex1));
        double up = 6*ex1;
        double down = Math.log10(ex2);
        double cos_on_sin = cos/Math.exp(sin);
        double second = Math.abs(cos_on_sin);
        result = up/down + second;
        System.out.println(result);
    }
}
